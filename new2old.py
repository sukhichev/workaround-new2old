#!/usr/bin/env python
# -#- coding: utf-8 -#-
#
# This is Russian description. English description see below.
# 
# Костыль для конвертации форматов вывода АЦП
# 
# Copyright 2017 Сухичев Михаил Иванович <sukhichev@yandex.ru>
#  
# Это свободная программа. Вы можете распространять, модифицировать,
# или выполнять эти действия одновременно на условиях
# cтандартной общественной лицензии GNU (GPL), выпущенной  Фондом 
# свободного программного обеспечения (FSF) версии 2, или 
# (на ваше усмотрение) любой более поздней версии;
# 
# Эта программа распространяется в надежде, что она будет полезной,
# но без каких-либо явных или подразумеваемых гарантии. Подробнее 
# смотри cтандартную общественную лицензию GNU (GPL).
# 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# 
# Workaround for convert output ADC
# 
# Copyright 2017 Сухичев Михаил Иванович <sukhichev@yandex.ru>
#  
# This program is free software; you can redistribute it and/or modify 
# it under the terms of the GNU General Public License (GPL) as published by the 
# Free Software Foundation (FSF); either version 2 of the License, or 
# (at your option) any later version;
#  
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

__version__ = "0.0.1" # Версия программы


import os
#import sys
import glob
import argparse

import re


def parse_options():
	""" Разбор опций
	"""
	
	parser = argparse.ArgumentParser(
		description = "\
Convert all file in current directory from new to old format")
	
	parser.add_argument("-r", "--reverse", action = "store_true",
                    help = "Reverse convert direction")
	
	group = parser.add_mutually_exclusive_group()
	group.add_argument("-s", "--suffix", action = "store",
                    help = "Setup suffix creating file")
#	group.add_argument("--in-place", action = "store_true",
#                    help = "Edit files in place")
# TODO: Для конвертации и заменой существующего файла следует реализовать 
# опцию "--in-place".
	
	parser.add_argument("-c", "--columns", action = "store", type=int,
                    help = "Add to this number of columns, if it is need")
	parser.add_argument("-p", "--placeholder", action = "store",
                    help = "Setup placeholder for wanting data")
	parser.add_argument("-v", "--version", action="version", 
					version="%(prog)s " + __version__)
	
	args = parser.parse_args()
	
	return args

def check_magic(regexp):
	""" Возвращает функцию, которая проверяет, что первая строка файла filename
	соотвествует регулярному выражению regexp
	"""
	p = re.compile(regexp)
	def closure(filename):
		with open(filename, "r") as f:
			if p.match(f.readline()):
				return (True)
			else:
				return (False)
	return closure

def outfilename(filename, suffix):
	""" Возвращает имя файла filename с добавленным суффиксом suffix
	"""
	elem = filename.split(".")
	return ".".join(elem[:-2] + [elem[-2] + suffix] + elem[-1:])
	

# Парсеры 

def old_parse(file_line):
	return re.split(':| ', file_line.strip())

def new_parse(file_line):
	return file_line.strip().split(' ')

# Обработка списка между форматами

def swap_by_list(swap_dict, placeholder):
	""" Возвращает замыкание, которое 
	создаёт список в котором элементы списка obj с номерами, 
	соответствующими ключам словаря swap_dict, находятся на местах 
	соответствующим значениям этого словаря. Пустующие места будут 
	заполнены заполнителем placeholder
	"""
	list_len = max(kv_swap(swap_dict))
	def closure(obj):
		ans = (list_len+1) * [placeholder]
		for i_in, i_out in swap_dict.items():
			ans[i_out] = obj[i_in] 
		return ans
	return closure

def add_to(n, placeholder):
	""" Возвращает замыкание, которое дополняет список obj до 
	n элементов, используя заполнитель placeholder
	"""
	def closure(obj):
		list_len = len(obj)
		if n > list_len:
			return (obj + (n - list_len) * [placeholder])
		else:
			return obj
	return closure

# Генераторы формата строк из списка

def new_write(out, temp):
	out.write(" ".join(temp) + "\n")

def old_write(out, temp):
	out.write("{:0>2}-{:0>2} {:0>2}:{:0>2}; {}\n".format(
		temp[0], temp[1], temp[2], temp[3], ";".join(temp[4:])) )

# Вспомогательные функции

def kv_swap(dic):
	return {v:k for k,v in dic.items()}

def no_edit(obj):
	return obj


def main():
	
	swap_dict = {3:2, 4:3, 6:0, 7:1, 11:5, 13:6, 14:8, 15:7, 16:9, 17:4}
		# Словарь соотвествия полей в новом и старом формате
	
	args = parse_options()
	
	placeholder  = args.placeholder if args.placeholder else "-"
	
	if args.reverse:
		check_file = check_magic("^\d{1,3}-\d{1,3} \d{1,2}:\d{1,2}; \d{1,4};")
		suffix = "(new)"
		in_parse = old_parse
		edition = swap_by_list(kv_swap(swap_dict), placeholder)
		out_write = new_write
	else:
		check_file = check_magic("^\d{1,3} \d{1,3} \d{1,2} \d{1,4} \d{2,4}")
		suffix = "(old)"
		in_parse = new_parse
		edition = swap_by_list(swap_dict, placeholder)
		out_write = old_write
	
	if args.suffix:
		suffix = args.suffix

	if args.columns:
		adder = add_to(args.columns, placeholder)
	else:
		adder = no_edit
	
	# Основной цикл
	
	for filename in filter(check_file, glob.glob("./*.*")):
		with open(outfilename(filename, suffix), "w") as out:
			for file_line in open(filename, "r"):
				temp = in_parse(file_line)
				temp = edition(temp)
				temp = adder(temp)
				out_write(out, temp)
	
	
	return 0

if __name__ == '__main__':
	main()

